# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- TypeScript, modern JavaScript features, and ES modules by default.
- JavaScript formatting, linting, type-checking, unit testing, bundling, compiling, legacy transpiling, and minification.
- PHP linting, type-checking, and unit testing.
- Git hooks to run code quality checks on commit, compile on push, and updated stale dependencies on pull.
- Bitbucket pipeline to run code quality checks and compile on pull request.

### Changed

### Deprecated

### Removed

### Fixed

### Security
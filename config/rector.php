<?php

declare(strict_types=1);

use Rector\Core\Configuration\Option;
use Rector\Core\ValueObject\PhpVersion;
use Rector\Set\ValueObject\DowngradeLevelSetList;
use Rector\Set\ValueObject\SetList;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function( ContainerConfigurator $container_configurator ): void {
	$container_configurator->import( DowngradeLevelSetList::DOWN_TO_PHP_80 );
	$container_configurator->import( DowngradeLevelSetList::DOWN_TO_PHP_74 );
	$container_configurator->import( DowngradeLevelSetList::DOWN_TO_PHP_73 );
	$container_configurator->import( SetList::CODE_QUALITY );
	$container_configurator->import( SetList::DEAD_CODE );
	$container_configurator->import( SetList::NAMING );
	$container_configurator->import( SetList::PSR_4 );
	$container_configurator->import( SetList::TYPE_DECLARATION );
	$container_configurator->import( SetList::TYPE_DECLARATION_STRICT );

	$parameters = $container_configurator->parameters();
	$parameters->set( Option::PHP_VERSION_FEATURES, PhpVersion::PHP_81 );
};

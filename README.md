# Modern PHP and TypeScript project boilerplate

[![PDS Skeleton](https://img.shields.io/badge/pds-skeleton-blue.svg?style=flat-square)](https://github.com/php-pds/skeleton)

Quickly get up and running with modern PHP, Node, and TypeScript using ES modules.

Code is automatically formatted, linted, bundled and compiled. Unit tests and mocks are available. And some other development workflow automations are built-in.

- [Quick start](#markdown-header-quick-start)
- [Getting started](#markdown-header-getting-started)
    - [Install Node and npm](#markdown-header-install-node-and-npm)
    - [Install PHP](#markdown-header-install-php)
    - [Install Composer](#markdown-header-install-composer)
    - [Install fswatch](#markdown-header-install-fswatch)
- [Dependencies](#markdown-header-dependencies)
    - [Code quality](#markdown-header-code-quality)
    - [Upgrading dependencies](#markdown-header-upgrading-dependencies)
        - [Node packages](#markdown-header-node-packages)
        - [Composer packages](#markdown-header-composer-packages)
    - [Dependency hygiene](#markdown-header-dependency-hygiene)
- [How to contribute](#markdown-header-how-to-contribute)
- [Requirements](#markdown-header-requirements)
- [Roadmap](#markdown-header-roadmap)

## Quick start

1. Install Node, npm, PHP, and Composer
2. Clone this repo (npm and composer dependencies will automatically install)
3. Run `. ./bin/quality.sh` to enforce code quality
4. Run `. ./bin/compile.sh` to compile source code

```bash
git clone git@bitbucket.org:paulshryock-nba/modern-php-and-typescript-project-boilerplate.git my-project
cd my-project
. ./bin/quality.sh
. ./bin/compile.sh
```

## Getting started

To run this project locally, first install the following software.

| Tool               | Min. version | Recommended version |
| :---               | :---         | :---                |
| Node.js            | 17           | 17.5                |
| npm                | 8.1          | 8.5                 |
| PHP                | 8.1          | 8.1                 |
| Composer           | 2            | 2.2                 |
| fswatch (optional) | 1            | 1.16.0              |

Then clone this repo, replace the project name and description, and remove this paragraph.

### Install Node and npm

See https://nodejs.org/en/download/package-manager/

### Install PHP

Homebrew: `brew install php@8.1`

See https://www.php.net/manual/en/install.php

### Install Composer

```bash
# Programmatically install Composer.
. ./bin/dependencies/install-composer.sh
```

See https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md

### Install fswatch

If you want to run PHP unit tests and watch for file changes, you'll need to [install fswatch](https://pestphp.com/docs/plugins/watch#installation) on your machine, or check if you already have it:

```bash
fswatch --version
```

## Dependencies

When you first clone this project, it will automatically run `npm install` and `composer install` to install all the necessary project dependencies.

### Code quality

These development tools are provided and configured to ensure code quality is automatically enforced during development, before committing code changes, before and after pushing code to a remote repository, and before deployment.

| Ecosystem | Tool                | Purpose                       |
| :---      | :---                | :---                          |
| Editor    | Editorconfig        | Code formatter                |
| Node      | Prettier            | JS formatter                  |
| Node      | ESLint              | JS linter                     |
| Node      | TypeScript          | JS type-checker and analyzer  |
| Node      | Jest                | JS test runner                |
| Node      | ESbuild             | JS compiler and bundler       |
| Node      | swc                 | JS transpiler                 |
| Node      | Sync dotenv         | .env.example automation       |
| Node      | Sort package.json   | package.json sorter           |
| Node      | Husky               | Git hooks                     |
| Node      | Release Bump        | Version bump automation       |
| Composer  | PHPCS               | PHP linter                    |
| Composer  | Psalm               | PHP type-checker and analyzer |
| Composer  | Pest                | PHP test runner               |
| Composer  | Mockery             | PHP mock framework            |
| Composer  | Skeleton            | File system structure         |
| CI        | Docker              | Local CI                      |
| CI        | Bitbucket pipelines | Remote CI                     |

### Upgrading dependencies

Whenever you `git pull` new commits from the remote origin, you'll be automatically notified of any available dependency upgrades. When you see that upgrades are available, you should upgrade and test locally, and commit those changes. Please do not include source code changes in the same commit along with dependency updates unless you are refactoring source code to accomodate new breaking changes in upgraded dependencies.

Or use [Renovate](https://renovatebot.com/).

#### Node packages

`npm-check-updates` is a helper tool for upgrading node package dependencies in `package.json`. Run the local version with `./node_modules/.bin/ncu` or install globally (`npm i -g npm-check-updates`) to use `ncu` directly.

Running `ncu` without any flags will perform a dry run to show what package upgrades are available. Running `ncu` with `-u` will modify `package.json`. After that, running `npm install` will modify `package-lock.json`.

It's a good idea to upgrade in phases and commit after each.

So for example, you might upgrade all **patch** versions with `ncu -t patch -u`, run `npm install`, and then commit your changes. Then you might upgrade minor versions the same way and commit. Finally, if you're ready for the major version upgrades, you would upgrade the major versions and commit those changes.

An alternate strategy would be to upgrade related packages together, such as `eslint` and all `eslint` plugins by filtering with `ncu -f eslint -u`.

| Command | Description | File modified |
| :--- | :--- | :--- |
| `ncu` | Check if updates are available. | N/A |
| `ncu -t patch -u` | Upgrade all available patch versions. | `package.json` |
| `ncu -t minor -u` | Upgrade all available minor versions. | `package.json` |
| `ncu -t latest -u` | Upgrade all available major versions. | `package.json` |
| `ncu -f some-filter -u` | Upgrade filtered packages by all available versions. | `package.json` |
| `ncu -f some-filter -t patch -u` | Upgrade filtered packages by patch versions. | `package.json` |
| `npm install` | Download upgraded dependencies, update lockfile. | `package-lock.json` |

#### Composer packages

To upgrade composer packages in `composer.json`, use `composer require` (with the `--dev` flag for devDependencies) followed by a space-separated list of packages to upgrade.

| Command | Description | File modified |
| :--- | :--- | :--- |
| `composer outdated -D` | Check if updates are available. | N/A |
| `composer require [--dev] some-package another-package` | Upgrade dependencies. | `composer.json` |
| `composer u` | Download upgraded dependencies, update lockfile. | `composer.lock` |

### [Dependency hygiene](https://technotes.khitrenovich.com/time-to-introduce-dependency-hygiene/)

Use lock files. [Pin](https://docs.renovatebot.com/dependency-pinning/) your [dependencies](https://medium.com/the-guild/how-should-you-pin-your-npm-dependencies-and-why-2b8d545c7312). Run audits. [Patch frequently – but don’t rush fresh upgrades in.](https://renovatebot.com/)

## How to contribute

See [contributing guidelines](../CONTRIBUTING.md).

## Requirements

This project should be:

- Accessible (a11y, assets on cdn, caching/offline-first, no giant cookies/headers, no frequent user lockouts, "self-service" whenever possible, etc.)
- Agnostic (platform, network, device, screen size, browser, user agent, etc.)
- Budgeted (https://github.com/GoogleChrome/lighthouse/blob/master/docs/performance-budgets.md#performance-budgets-budgetjson)
- Continuously deployed (feature flags to hide incomplete/early features)
- Continuously integrated (Code formatting, linting, type-checking, testing, auditing, building etc.)
- Documented (code, documentation, CMS UI)
- Ethical (https://www.ethicalweb.org/)
- Inclusive (i18n, a11y, inclusive form fields and language, etc.)
- Modern (ESM, ES2021, latest TypeScript/Node.js/npm/PHP versions, etc.)
- Performant (lightweight, minimal dependencies, cookies, requests, etc.) (https://www.yunier.dev/post/2021/running-lighthouse-in-cicd-pipeline/)
- Responsive (mobile-first, etc.)
- Secure (HTTP headers, firewall, auth, etc.)
- Semantically versioned
- Stable (audit, test, qa before release)
- Test-driven (unit, integration, E2E)

## Roadmap
- Add Docker for containerized local CI
- Enforce Node, npm, and PHP versions
- Fail build if code coverage is below minimum required
- Configure Xdebug for PHP unit test coverage reporting
- Add [Cucumber.js](https://www.npmjs.com/package/@cucumber/cucumber) and [Behat](https://docs.behat.org) for [BDD](https://en.wikipedia.org/wiki/Behavior-driven_development#Behavioral_specifications)
- Add [Renovate](https://www.whitesourcesoftware.com/free-developer-tools/blog/automated-dependency-updates-for-bitbucket-cloud/) for automated CI dependency upgrades.
- Add [Signon](https://sinonjs.org/) for JS test spies, stubs and mocks
# WordPress

## Official
- [Official WordPress Developer Resources](https://developer.wordpress.org/)
- [Code Reference](https://developer.wordpress.org/reference/)
- [Coding Standards](https://developer.wordpress.org/coding-standards/)
- [Block Editor Handbook](https://developer.wordpress.org/block-editor/)
- [Common APIs Handbook](https://developer.wordpress.org/apis/)
- [Theme Handbook](https://developer.wordpress.org/themes/)
- [Plugin Handbook](https://developer.wordpress.org/plugins/)
- [REST API Handbook](https://developer.wordpress.org/rest-api/)
- [WP-CLI Commands](https://developer.wordpress.org/cli/commands/)
- [wp-env](https://developer.wordpress.org/block-editor/reference-guides/packages/packages-env/)

## Unofficial
- [WordPress The Right Way](https://www.wptherightway.org/)
- [WordPress | Smashing Magazine](https://www.smashingmagazine.com/search/?q=wordpress)

### Hooks
- [How to remove WordPress Hooks that use Closures and Object Methods](https://inpsyde.com/en/remove-wordpress-hooks/)

## Forums / Q&A
- [WordPress development on StackExchange](https://wordpress.stackexchange.com/)
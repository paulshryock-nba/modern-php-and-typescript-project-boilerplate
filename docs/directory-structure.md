# Directory structure

This project's directory structure adheres to [pds/skeleton](https://github.com/php-pds/skeleton#summary), a standard defined by the [PHP-PDS](http://php-pds.com/) (PHP Package Development Standards) initiative.

## Summary

```bash
# tree -daL 1
.
├── .git         # git files
├── bin          # command-line executables
├── cache        # cache files (do not commit)
├── config       # configuration files
├── coverage     # test coverage reports (do not commit)
├── dist         # compiled application code (do not commit)
├── docs         # documentation files
├── node_modules # node dependencies (do not commit)
├── public       # web server files
├── resources    # other resource files
├── src          # source code
├── tests        # test code
└── vendor       # composer dependencies (do not commit)
```

## `.` (root)

The root directory contains text files describing the repo itself, environment variables, git files, configuration files, CI files, and package manager files.

```bash
.
│   # text files
├── CHANGELOG.md            # notable changes of each version of the project
├── CONTRIBUTING.md         # contributing guidelines
├── LICENSE                 # use and distribution guidelines
├── README.md               # what the project is and how to use it
│
│   # environment variables
├── .env                    # your local environment variables
├── .env.example            # environment variable examples
│
│   # git project files
├── .gitattributes          # git path attributes
├── .gitignore              # untracked files to ignore
│
│   # configuration files
├── .editorconfig           # Editor config
├── .eslintrc.cjs           # ESLint config
├── .node-version           # Node version (used by some version managers/CI)
├── .npmrc                  # npm config
├── .nvmrc                  # nvm config (used by some version managers/CI)
├── .phpcs.xml              # PHP_CodeSniffer config
├── .prettierrc.cjs         # Prettier config
├── jest.config.cjs         # Jest config
├── phpunit.xml             # Pest config
├── psalm.xml               # Psalm config
├── tsconfig.json           # TypeScript config
│
│   # CI files
├── bitbucket-pipelines.yml # Bitbucket pipelines
│
│   # package manager files
├── composer.json           # Composer
├── composer.lock           # Composer lock file
├── package-lock.json       # npm lock file
└── package.json            # npm
```

## `bin` (command-line executables)

todo

## `cache` (cache files)

todo

## `config` (configuration files)

todo

Some configuration files must stay in the root directory in order to function correctly. All other configuration files should go in the `config/` directory, to keep the root directory clean where possible.

## `coverage` (test coverage reports)

todo

## `dist` (compiled application code)

todo

## `docs` (documentation files)

Project documentation for software engineers.

## `node_modules` (node dependencies)

todo

## `public` (web server files)

todo

## `resources` (other resource files)

Relevant engineering resources for project contributors.

## `src` (source code)

todo

## `tests` (test code)

todo

## `vendor` (composer dependencies)

todo
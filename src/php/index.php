<?php
/**
 * This file can be replaced or removed.
 *
 * @package project-name
 */

// Autoload project dependencies.
require '../../vendor/autoload.php';

// Load environment variables.
$dotenv = Dotenv\Dotenv::createImmutable( dirname( __DIR__, 2 ) );
$dotenv->safeLoad();
$dotenv->required( [ 'BUILD_ENV', 'BUILD_VERSION' ] );

/**
 * Build environment.
 *
 * @var string
 */
$build_env = filter_input(
	INPUT_ENV,
	'BUILD_ENV',
	FILTER_VALIDATE_REGEXP,
	[ 'regexp' => '(local|dev|qa|uat|prod)' ],
) ?: 'local';

/**
 * Build version.
 *
 * @var string
 */
$build_version = filter_input(
	INPUT_ENV,
	'BUILD_VERSION',
	FILTER_VALIDATE_REGEXP,
	[
		'regexp' =>
			'(unreleased|\d*\.\d*\.\d*(-(nightly|alpha|beta|rc)\.\d*)?)',
	],
) ?: 'unreleased';

echo esc_html(
	sprintf(
		"\$build_env: {$build_env}, \$build_version: {$build_version}",
		$build_env,
		$build_version,
	)
);

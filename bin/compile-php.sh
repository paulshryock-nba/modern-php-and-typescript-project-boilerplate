#!/bin/sh

# Get packages requiring PHP above 7.3.
composer install --no-dev --no-progress --ansi
# todo: Filter out current project.
packages=$(composer why-not php "7.3.*" | grep -o "\S*\/\S*")

# Install all dependencies.
composer install --no-progress --ansi

# Get paths to packages requiring PHP above 7.3.
for package in $packages
do
	# todo: Prepend dist/ directory.
	path=$(composer info $package --path | cut -d' ' -f2-)
	paths="$paths $path"
done

# Compile PHP down to 7.3.
vendor/bin/rector process --config config/rector.php --no-diffs --no-progress-bar dist $paths

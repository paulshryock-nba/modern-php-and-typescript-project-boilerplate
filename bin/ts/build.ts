import { dirname, join } from 'path'
import { fileURLToPath } from 'url'
import { $ } from 'zx'
import pkg from '../../package.json' assert { type: 'json' }

const { BUILD_ENV, BUILD_WATCH } = process.env
const WATCH = BUILD_WATCH === 'true'
const { version } = pkg
const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

const paths = {
	dist: 'dist',
	js: {
		src: join(__dirname, '../../src/ts/main.ts'),
		dist: join(__dirname, '../../dist/theme/js/main.js'),
		legacy: join(__dirname, '../../dist/theme/js/main-legacy.js'),
	},
}

/** Environment variables. */
interface EnvironmentVariables {
	/** Build environment. */
	BUILD_ENV: string
	/** Build version. */
	BUILD_VERSION: string
	/** Build watch. */
	BUILD_WATCH: boolean
}

/**
 * Compiles TypeScript.
 *
 * @since unreleased
 */
async function compile(): Promise<void> {
	/** Environment variables. */
	const env: EnvironmentVariables = {
		BUILD_ENV: BUILD_ENV ?? 'local',
		BUILD_VERSION: version,
		BUILD_WATCH: BUILD_WATCH === 'true',
	}

	// Compile TypeScript to ES2015 and bundle ES modules.
	await $`esbuild ${paths.js.src} \
		--bundle \
		--define:process=${JSON.stringify({ env })} \
		--format=iife \
		--minify \
		--outfile=${paths.js.dist} \
		--platform=browser \
		--sourcemap \
		--target=es2015 \
		${WATCH ? '--watch' : ''}`

	// Transpile bundled ES2015 to ES5 for legacy browsers.
	await $`swc ${paths.js.dist} \
		--config-file ./config/.swcrc \
		${WATCH ? '--log-watch-compilation' : ''} \
		--out-file ${paths.js.legacy} \
		--quiet \
		--source-maps \
		${WATCH ? '--watch' : ''}`
}

// Initialize build.
compile()

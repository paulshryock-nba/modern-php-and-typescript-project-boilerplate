# Audit production dependencies.
echo "📦 auditing production dependencies."
npm audit --omit=dev

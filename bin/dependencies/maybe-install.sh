#!/bin/sh

# Checks if a file has changed since the previous commit.
# @param {string} $1 The file to check.
function changed() {
	git diff --name-only HEAD@{1} HEAD | grep "^$1" > /dev/null 2>&1
}

# Maybe installs npm dependencies.
function npm_maybe_install() {
	NPM_MISSING=$([ ! -d "node_modules" ])
	NPM_CHANGED=$(changed 'package-lock.json')

	if $NPM_MISSING || $NPM_CHANGED; then
		if $NPM_MISSING; then
			echo "📦 installing npm dependencies."
		elif $NPM_CHANGED; then
			echo "📦 updating npm dependencies."
		fi
		npm ci
	fi
}

# Maybe installs composer dependencies.
function composer_maybe_install() {
	COMPOSER_MISSING=$([ ! -d "vendor" ])
	COMPOSER_CHANGED=$(changed 'composer.lock')

	if $COMPOSER_MISSING || $COMPOSER_CHANGED; then
		if $COMPOSER_MISSING; then
			echo "📦 installing composer dependencies."
		elif $COMPOSER_CHANGED; then
			echo "📦 updating composer dependencies."
		fi
		composer install
	fi
}

# Maybe install dependencies.
# Run in parallel and wait for both to finish.
npm_maybe_install & \
composer_maybe_install & \
wait

#!/bin/sh

# Check for outdated dependencies.
# Run in parallel and wait for both to finish.
echo "📦 checking for outdated dependencies."
./node_modules/.bin/ncu & \
composer outdated -D & \
wait

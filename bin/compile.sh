#!/bin/sh

# Compile source code.
# Run in parallel and wait for both to finish.
composer run build & \
npm run build & \
wait

#!/bin/sh

# Enforce code quality.
# Run in parallel and wait for both to finish.
composer run quality & \
npm run quality & \
wait

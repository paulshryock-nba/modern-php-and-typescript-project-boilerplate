/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
	modulePathIgnorePatterns: ['vendor'],
	preset: 'ts-jest',
	resolver: 'jest-ts-webcompat-resolver',
	testEnvironment: 'node',
}
